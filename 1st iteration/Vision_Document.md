Table of Contents

[[_TOC_]]

# Revision History
| Date | Version | Description | Author |
| ----------- | ----------- | ----------- | ----------- | 
| <25/02/2022> | <1.0> |  1st iteration: Fleshing out the start of the vision document filling out introduction, positioning, project goals, stakeholder and user descriptions, product overview, product features and constraints.| Group 16 |
| <07/03/2022> | <2.0> |`2nd iteration: Added some new definitions to section 1.2, some adjustment made to section 5.4, added content to 1.4, section 9, edited section 4.5,| Group 16
|<27/04/2022> | <3.0> | 3rd iteration: Adding content to section 11 and 12, removing some sections (like section 10 and 10.1, 11.4), quality checks and finishing up the rest of the document | Group 16 | 


# Vision
## 1. Introduction
This document is a reflection of our groups and our customer's vision of what product we want to make for the project. The project involves the creation and implementation of a tournament bracket generator. Our customer is a e-sports player so our software will primarily be catered towards appealing to the e-sport marked but the functionality will be there for other sports as well. The project kicked off on February 23rd, with a deadline for the assignment on April 29th and presentation by May 6th. And during this period, we are to present our product in three iterations. Specifics about the iterations can be found later in the document. 
### 1.1 Purpose and scope
This vision document are supposed to reflect the purpose, plans and scope of this project. We have been assigned a project where we are to create a tool for setup and result service for a multi-team tournament (chess, handball, badminton, volleyball, e-sports, quiz or similar). We have chosen e-sport tournaments for our scope, what products and options to offer for this kind of tournament, and the different requirements that must be met to be able to satisfy our customer. 
### 1.2 Definitions, Acronyms, and Abbreviations
| Definitions, Abbreviations, Acronyms | Description |
| ------------------------------------ | ----------- |
| Round Robin | In a tournament setting, a round robin is a competition in which each team plays against all other teams, usually in turn, the same amount of times. The winners of the round robin are the teams that score the highest in their respective group. |
| Group play | Set to be used in conjunction with the Round Robin format where the teams are placed into groups and then play a round robin style matchup within their group. |
| Double elimination | Double elimination gives every team/participant a seeding and places them in a bracket the participants then square of in one vs one knockout matches where the winner moves on and the loser moves to the loser bracket and can win his way back up to the finals. In other words, if you lose twice, you are out.  |
| Single elimination | Single elimination gives every team a seeding and places them in a bracket. The participants then square off in a one versus one knockout match where the winner moves on and the loser is out of the tournament after one elimination. |
| Seed(ing) | In the context of a tournament, a seed is a competitor in a (sport) tournament who is given a preliminary ranking for the purposes of the draw [1]. A seeded tournament is set up so that the highest ranked team meets the lowest ranked team in the beginning. The intention is for the best ranked teams to meet each other at a later point in a competition. |

### 1.3 References
[1] Definition of seeding. Wikipedia, found 01.03.2022: https://en.wikipedia.org/wiki/Seed_(sports) 
### 1.4 Overview
For the rest of this vision document, we will include what is relevant for our project, as some sections are more relevant than others. We have either removed some sections or merged them with others, to make it more structured. Some of the most important sections includes the goal settings, product overview and features, risks, costs and the relations to our stakeholders.  
## 2. Positioning
### 2.1 Business Opportunity
The business opportunity met by this project is that we can sell our product to potential buyers matching our demographic. It can be everything from a small, local tournament or a large scale, worldwide tournament.
### 2.2 Problem Statement
The problem of having tournaments, large-scale or small-scale, is that one can quickly lose track of which player/ team is competing against the other. One could also have difficulties keeping up with the scores of the individual player/teams. 

This affects not only players and viewers, but also the administrators, whose job it is to keep track of all this information.  

The impact of this is that no one would know who’s in the lead or who’s competing against who. It would be a chaotic and confusing tournament, which could quickly lead to quarrels and disagreements. It creates unwanted waiting time which could be avoided. 

A successful solution would be to have a system that lets an administrator effectively add or remove teams, set teams up against each other, have an easy system which lists how many point the player/ team have. 
### 2.3 Product Position Statement
Anders Lillengen will be our main client for this product. The tournament bracket will be name “The group 16 e-sport bracket”. It is a person/ team and number track-keeping service, that lets an administrator fast and easy add the necessary information needed for a tournament. Unlike Challonge, our product makes keeping track of a tournament an easy, straightforward, and perhaps even a fun task. Primary differentiation is that Challonge needs an internet connection, whereas our product is C++ based, and therefore works everywhere. No internet access needed.   

## 3. Project Goals
First and foremost, we aim to deliver the project and present it to the client on time, with what we and the client agree on, to make an approved and desired product. Our product should fulfill all agreed requirements with the goal of pleasing the customer. 


During the project we are also obligated to show the client our progress during scheduled meetings and be open to any changes or further requirements the customer may want. To get as much information out of the client as possible it is important to present a well-prepared product, and present further thoughts of the vision for the product. Good communication with the customer is therefore important. 
### 3.1 Impact Goals
When it comes to this project and impact goals, we want the goals to reflect the long-term effects. They will be focusing on reflecting the goals of this course, and what we want to achieve with the assigned project. The impact goals we are aiming to fulfil are as following: 

- To work effectively and purposefully in a team towards a common goal 

- To absorb as much knowledge as possible on the process of creating software 

- To build a tool that is intuitive and user friendly, so that we can win more customers 

- Improve our presentation skills, project- and collaboration skills, and gain knowledge about software engineering. 

- To expand our network  

- The possibility of more options on the market for these types of activities.  
### 3.2 Result Goals
We have already set some impact goals for this project. The result goals will outline what we need to do to achieve these. For the result goals, we have the following we want to achieve during this project: 

- Each group member will spend about 110 hours (+/- 10%) in total to complete this project on time. 

- We want to deliver a product that lives up to the expectations of our customers, and at the same time, deliver on the requirements for this project. 

- We want to draw more users to our product.
## 4. Stakeholder and User Descriptions
When working on a project and making a product for a client, it is important to understand the industry and how things work. Hence, identifying the stakeholders involved will be a key element for a successful implementation.  
 
Relevant stakeholders will be everyone affected by this project. It will mainly be the administrator that has to keep the scores and keep tracks on who is battling who, and other relevant users of this tournament tool, and of course the audience and participants within e-sport. 

The software will be created based on both the requirements set in the project and the feedback from our main user Anders Lillengen needs and wishes. It will be created to be more flexible for others as well. We want it to be simple and inclusive for all types of users. The program will be available on the internet as well, so the users is everyone deciding to use our software for creating tournaments. Section 4.2 and 4.3 will provide more details about the stakeholders involved in this project. 
### 4.1 Market Demographics
Our target group will be everyone who is interested in e-sports regardless of gender, age and lifestyle. We think it is important to include everyone who has an interest in e-sport, and the neutral and simple design of our product will reflect this. Since this is our very first project within software development, it is fair to assume that our reputation as a group for now is pretty non-existent. 
However, NTNU has a great reputation as a respected institution. NTNU is known for being one of the best universities in Norway and attract some of the best students from around the country. A program made by tech-students from a highly recognized study program will potentially draw positive attention from users, fans and sponsors.  
### 4.2 Stakeholder Summary 
There are a number of stakeholders with an interest in the development and not all of them are end users. Below we find a summary list of these non-user stakeholders. (The users are summarized in section 4.3.) 

| Name | Description and responsibilities |
| --- | --- |
| Course Lecturer | Course lecturer gave us this assignment and will be the final judge of our work. |
| The audience | They will decide on the experience |
| Marketing team |  Promoting the tournament and ensure market demand for the product |
| Sponsors | Sponsoring the tournament and approves funding |
| Event Operators | Organize and host the event |

### 4.3 User Summary
Below we find a summary list of all identified users. 

| Name | Description and responsibilities |
| --- | --- |
| Administrator  | Administrator of the bracket tool we make. |
|Anders Lilleengen and other end users|Our customer will provide feedback for our product|
|IT-department|Maintaining the bracket tool system|
|Players (single or team)|The system must be easy for them to understand, as they will follow the instructions |


### 4.4 User Environment 
Detail the working environment of the target user, the administrator. Here are some suggestions: 

- Number of people involved in completing the task? Is this changing? There will be one administrator at a time, to handle the program and registering. This might change, due to sickness and other absence. The information registered will be stored through persistence (read/write to/from file), so if another administrator takes over a current tournament, this will not be an issue. It will also be ready for a new tournament when the current one is done.  

- How long is a task cycle? Amount of time spent in each activity? Is this changing? The time spent on a task cycle may vary, based on the task itself. The tasks may take between 1-2 weeks per task.  

- Any unique environmental constraints: mobile, outdoors, in-flight, and so on? The use of mobile phones should be limited, as the main focus will be on the project task and delivering on the agreed requirements. During breaks, it can be used freely. Urgent phone calls are also acceptable.  

- Which systems platforms are in use today? Future platform?  
Our customer could not recall the specific names for the platforms used for tournaments today. But mainly in leagues, there are group games (double round robin or something like that), and then they drive it into a win-or-lose bracket. Double elimination when it's play-off, so they have a loser bracket as well.  
### 4.5 Key Stakeholder or User Needs
Here we will address the key problems with existing solutions as perceived by the stakeholder or user.  
 

- What are the reasons for this problem?  
 
Some of the key problems with the wireframe is the naming of the different options and some of the functionalities within each option. The users had most issues with the options for “tournament” and “register win”. The issue regarding the tournament option is that it was unclear what this option was supposed to portray. Was it the match setup and who is playing against who? Regarding “register win” we also had an option called “standings” and some of the users did not quite understand the difference between these two, as they sound similar. A more visual portrait of the entire bracket was also missing in the tournament option, according to some users (that shows who is out and who will move further to the next level, etc).  The main customer and some other users want seeding options in the bracket, which was not visible in the prototype or MVP. Another point made to us during the 2nd client meeting with our lecturer was that we need to include persistence in our program (read/write to/from file) as this is important for a tournament bracket, especially when updating scores and results.  
 

- How is it solved now? 

 
Some of the options from the first prototype are removed or adjusted. We removed the “standing” part, as this can be implemented and simplified in the option for “register win”. Some minor adjustments, e.g, making an arrow button to take you back to the homepage under each page. Considering our MVP, we had to simplify and remove some options and we will prioritize to fulfil our requirements for the MVP. For the final product, we added seeding option for each team, as this was a strong wish from our main customer. We will also in the final product include program persistence.  
 

- What solutions does the stakeholder or user want? 
 
Ideally, the customer and some of the users would have had group games (double-round-robin best of 3) into double-elimination. Because then you get to eliminate the worst teams relatively quick. Say you have 4 teams in each group and then 2 go on. If the good teams mess up in the win bracket, then they get an extra chance in the loser bracket. 
The main customer would also like the possibility of adding multiple games late in the tournament, eg 5 matches in the playoffs, versus single-elimination or double-elims earlier. Then the audience will also be happy because they get to see better matches. And the players get some fair competition. This may be too advanced for the program we are going to make, due to limitations in time and skills. The table below will rank their wishes and our priorities. 

| Need | Priority | Concerns | Current Solution | Proposed solutions |
| --- | --- | --- | --- | --- |
| Competing team scores (win, lose, draw) | Medium | It is a part of the tournament bracket, but must discuss the technical aspects and simplify the code. | We have to manually type in winner and loser | Proposed to either remove this option for MVP, or make it simpler. For the final product, we will manually register winner/loser, and this will be stored and used for the next round/finale. |
| Double elimination and round robin | Low | Will be too advanced for our MVP and final product | In our first prototype, we included double elimination and round robin | Will focus on single elimination only, for both MVP and final product. |
| More intuitive names for the some of the options in the wireframe | High | The options “tournament” and “register win” was a little confusing to some of the users, as it was unclear what they contained. | We chose to have the match setup under tournament, and the final scores/results under “register win”. | We have removed the option “name the teams” and “standings” in the wireframe, to simplify our MVP. For the final product, we will give the user the option to name teams, and choose total amount of team competitors per tournament (4,8,16).|
| Seeding | High | Seeding is considered to be an important feature in a tournament. For the sake of the audience and the quality of the tournament itself. | Seeding will not be included in the MVP. | Seeding will be added to the final product, and not the MVP. |

### 4.6
Some of the competitors we have identified are the following:
#### 4.6.1 Challonge
Global and well known, a huge user base, has a tournament simulator, nice graphic design.  
Cons: They charge for their services, and you need internet connection to use this platform. Gives fans a chance to see the relative standigs as a spectator as the games progress.
#### 4.6.2 Toornament
Can specify your own tournament rules. Also provides management solution to esports organizers.  
Cons: Not easy to find information about membership pricing. Website also less aesthetically pleasing for gamers, as many prefer “dark mode” on the websites. Also needs internet connection for this platform.
## 5. Product Overview
This section provides a highlevel view of the product capabilities, interfaces to other applications, and system configurations. This section usually consists of three subsections, as follows:  

- Product perspective  

- Product functions 
### 5.1 Product Perspective
In perspective to other related products, e.g Challonge, the products are similar. E.g our product will be able to let an administrator create and maintain a tournament, just as a competitor, such as Challonge. However, our product is independent, it is not reliant on other systems or products, it does not need any databases or other systems to help it do what it is supposed to do. This makes it so that our product is, among other things, easier to install and use immediately.  
### 5.2 Summary of Capabilities 
| Customer Benefit | Supporting Features |
| --- | --- |
| Creation and editing of tournaments | Allows the tournament organizer to create new tournaments and to administer the name and type of the tournament |
| Display tournament brackets | Displays the tournament bracket and the current standings of the different team based on the tournament type |
| Add participating teams and seed them into the brackets | Allows tournament organizers to add participating teams to the tournament as well as give them different seeding positions before starting the tournament based on their overall rating. |
| Single and double elimination | Allows the tournament organizer to create a single or double elimination tournament based on registered teams |
| Round robin/pool play | Allows for a different type of competition (if this proves to be too hard to implement we will not make this) |
| Tournament schedule | Allows for users and Tournament organizers to see the time in which the matches are to be played |

### 5.3 Risk analysis 
As with everything in life in general, there is always a certain risk that something will bring and cause inconveniences. This project is no exception. There are several risk factors ranging from the work environment and the reliability of each team member, the collaboration and communication between the customer and the team, and of course, risk regarding deliverance, product and time management. We will list up the risk factors we believe will affect us to a certain degree. 
 
The risk factors identified so far are, or can be, the following: 
 
1. The risk of some of the team members dropping out of this course for different reasons, hence withdrawing from this project. There have been some members missing for a while already, for unknown reasons, that has caused some stress, extra workload on the rest, and in all lower productivity. 
However, everyone are back now and we expect that the risk decreases the closer we get to the submission deadline, as it would be a waste of time to work hard on the project, and then withdraw in last minute.  
 
2. There may be challenges with delivering everything on time, as we have other courses that are very time consuming as well. So far, we have been able to keep up with the deadlines, and by continuing to manage our time well and communicate clearly, we should be able to deliver everything on time. 

3.  There may be difficulties in obtaining the expertise or resources within the team, as all of the team members are relatively fresh students within tech. We rely on the feedback and guidance from our teaching assistant and lecturer, as well as seeking out the required knowledge. 
 
4. There is a risk of shortcomings and technical issues in the systems, as technology in itself can be unstable.  
 
5.  Code issues can be a potential big issue, as poor code quality will severely degrade the quality of the product. If our product does not work, we will not be able to deliver, hence it will ruin our project. We should focus on identifying bugs and errors as early as possible and fix them quickly. 

6. Inaccurate estimates from our side related to costs, miscommunication within the team and between our user(s), deadlines, etc, can lead to unmet expectations. We must strive to meet the requirements, and at the same time, keep it realistic. 
 
7.  External risks can also have an impact on our projects. This can be everything from changes in laws to pandemics causing a halt in the development and distribution. These are out of our control, but they are considered to be less likely than the other risks mentioned.  
 
8. How users and the audience respond is another risk to take into consideration. If the users are happy, the better reputation we will gain, and more users will use our product. 
 
9. The stakeholders can or will have a large effect on the success of our product, so forming good relations with our stakeholders and keep them engaged will be essential. 

 

 
Below, we have made a diagram to illustrate the risk factors mentioned above. These are classified and colored after their calculated risk, ranging from low (green), medium (yellow), high (orange) to critical (red). The numbers within each bubble reflects each risk factor as listed above: 

![Risk Diagram](https://i.imgur.com/Q40ioI5.png)
### 5.4 Cost, Pricing and benefits
Since the project will be developed as an open-source project and only will be offered digitally, there are almost no expenses when it comes to distribution of the software. An argument could be made that there already are tournament software freely available that can take the place of our software and that this could halter the pricing of our product. However, since this software is specified with certain criteria set by our customer it is streamlined for use in a specific setting which gives our product an advantage over other competitors. 
### 5.5 Quantifiable and non-quantifiable benefits
We are making a product, and from this product, we are expecting results. Not just results from the finished product itself, but also results regarding everything related to and surrounding the product. This includes quantifiable and non-quantifiable benefits. These are the quantifiable and non-quantifiable benefits expected as a result of the project’s finished product and the quantifiable and non-quantifiable benefits expected from the duration of the project’s worktime. 

Example: 

Impact goals: 

- Streamline the customer's tournament creation  

- Allow for easier scheduling of matches and administration of results 

Quantifiable benefits: 

- The quality of our product in relation to what we set out to do 

- Our grade regarding PROG1004 

Non-quantifiable benefits: 

- Our knowledge regarding software development 

- Our knowledge and experience regarding teamwork 

- Relations we have gotten towards each other during the project duration 

### 5.6 Estimated costs
The cost will be estimated using an hourly rate. We will estimate the hours needed to complete this project.  

It is expected that each of the group members spend 10-12 hours each week on this program which adds up to a total of approximately +/- 110 hours for each member throughout the entire project. The hourly rate at which we bill for our services are set from the project description at 1599 NOK/H. The sum of the time spend will then be set at 879 450 NOK for the entirety of the project. 

| Activities | Hourly rate | Hours | Costs |
| --- | --- | --- | --- |
| Felix | 1599 NOK | 110 | 17 890 NOK |
| Aron | 1599 NOK | 110 | 17 890 NOK |
| Donia | 1599 NOK | 110 | 17 890 NOK |
| Sebastian | 1599 NOK | 110 | 17 890 NOK |
| Andreas | 1599 NOK | 110 | 17 890 NOK |
| Sum |  |  | 879 450 NOK |

### 5.7 Licensing and Installation
We will use an open-source license for this project, since we think that it is good for continuous development of the project after we are done with it. 




MIT license 

 

Copyright 2022 Group 16 

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: 

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. 

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. `

## 6. Product Features
Below, we list up the planned functions for our product based on the user feedback and the requirements to a tournament bracket: 
### 6.1 TournamentCreation
- Writes the menu for team creation 

- Tells the other function to read in the data 
### 6.2 tournamentReadData
- Start reading in data for the tournament 

- Tournament name (string),  

- Tournament type (enum),    

- Delayed endgame (bool) (lav prioritet) 
### 6.3 tournamentWriteData
- Writes the data of the tournament 

- Tournament name 

- Tournament type 
### 6.4 newTeam
- Uses data sent from tournament creation to either read teams from a file or from the CLI interface 

- Calls on another feature to read in the data for the teams 
### 6.5 teamReadData
- Start reading in data for the team 

- Team name (string) 

- Team seed (int) 

- Win/loss (vector) 
### 6.6 teamWriteData
- Writes out the data about the team created 

- Team name 

- Team seed 
### 6.7 teamsStandings
- Calculates the standings of the current teams 

- Writes out the current standings based on the win/loss of each team 
### 6.8 newBracket
- Uses the teams and tournament data: win/loss, seed and tournament type. to create a tournament bracket. 

- Writes out corresponding bracket based on earlier read data through tournament creation 
### 6.9 singleWriteData
- Writes out the bracket as single elimination 
### 6.10 doubleWriteData
- Writes out the bracket as double elimination 
### 6.11 poolWriteData
- Writes out the bracket as pool play/round robin 
### 6.12 teamRegisterWin
- Uses other function to find corresponding team and add either a win or a loss to their data 

- Automatically adds win/loss to opponent 
### 6.13 findTeam
- Finds a team based on the data that is sent in to this funciton and returns it to the other function 
### 6.14 findOpponent
- Sends inn a chosen team and finds the opponent of the corresponding team. 

## 7. Constraints
There are some constraints in regard to the design of the console application, as it does not offer all our desired features, and makes it simpler than what the main client wished for. Our product is based on the requirements set from our course lecturer, as well as the feedback from our main client Anders, our lecturer and 4 other users. As freshmen students, our technical competence will be less than the skills possessed by professional programmers, hence the functions will be of a simpler character.  

## 8. Quality Ranges
Because the product is created in C++, the quality range for performance is limited to the programming language. Because it’s based on C++, it’s likely that the product will be able to run on almost every computer with little to no issues regarding performance. 

Regarding robustness, the product will have all the limitations C++ have.  Since C++ programs creates a step-by-step procedure, the program will unfortunately have a low fault tolerance. Meaning that it will be hard for the user to go back and correct misspelled words and wrongdoings once it's done. 

However, since C++ is a simple programming language, that also means the product will have an easy usability.      

## 9. Precedence and Priority
Our first priorities should be to create a strong base for the program to stand on, in which we can build features of if we want. The priorities are written in order: 

- Create tournament with teams 

- Have overview over teams and have a working scoring system 

- Single elimination bracket system 

## 10. Documentation Requirements
This section describes the documentation that must be developed to support successful application deployment.

### 10.1 User Manual
The purpose of the User Manual is to describe to the user what the different pages of the program do, what the different options do, how to use all the available options correctly and making sure there is no doubt in the user about how to generally move around in the program. 

The contents of the User Manual are the following: 

Introduction: This is an introduction to the User Manual. 

Purpose: Subclass of Introduction. Explains the purpose of the User Manual. 

Related Documents: Lists all the related documents, which is only the installation manual. 

Overview: General overview of the program. Shows the main menu and explains the options. 

Functional Description: Explains how to select the different options 

Instruction: Explains how to use the program. 

Since the program is not of significant length, size or complexity, the User Manual will reflect that. The user manual is only 4-5 pages. The points in the User Manual describing how to use the program is rather simple, because, since previously mentioned, the program itself is simple. Therefore, the need for a complex User Manual is not needed. 

### 10.2 Online Help

As the program is very straight forward with how it is used, and a detailed user manual is issued. The use for online help is not present. If we in the future should add many different options to make the program more advanced for the user, online help would be something we would consider. 

Many applications provide an online help system to assist the user. The nature of these systems is unique to application development as they combine aspects of programming (hyperlinks, and so forth) with aspects of technical writing, such as organization and presentation. Many have found the development of an online help system is a project within a project that benefits from up-front scope management and planning activity. 

### 10.3 Installation Guides, Configuration, and Read Me File
A Read me file will be included with the download. It will show the current version and include a user manual. 

## 11.  A   Feature Attributes
Features are given attributes that can be used to evaluate, track, prioritize, and manage the product items proposed for implementation. All requirement types and attributes need to be outlined in the Requirements Management Plan, however, you may wish to list and briefly describe the attributes for features that have been chosen. The following subsections represent a set of suggested feature attributes. 
 
 
The features given in previous sections above will be discussed in this section. 


### 11.1    A.1     Status
Set after negotiation and reviewed by the team members. Tracks progress during definition of the project baseline. 

| Status | Features |
|--------|----------|
| Proposed | Seeding for teams, Delete functionality in the program, Update module names, System clear function, Single elimination, Double elimination and round robin |
| Approved | Seeding for teams, System clear function, Single elimination, Delete functionality |
| Incorperated | See the "Approved" section |

### 11.2    A.2     Effort
The MVP proved to be a time-consuming process, and we can therefore assume that the final product will require a lot from us as well. The final product will take time to make and we will need all members to contribute to completing this. All functions and features will be listed in our project planner, and we will work on them, prioritizing the most important ones first. According to Gantt, we plan on spending 3 weeks on this process. 

### 11.3    A.3     Target Release
The target release of our final product will be the same time for when this entire project is to be submitted: Friday, April 29th.





