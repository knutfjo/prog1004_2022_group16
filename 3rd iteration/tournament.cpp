/**
 *  Tournament Program Group 16
 *
 *  The program:
 *  This program can be used to set up single elimination tournaments
 *  It allows for the creation on tournaments and registration of teams in the
 *  tournament. It also allows you to display the tournaments brackets
 *  aswell as updating the standings of the teams to determine a winner.
 *
 *  @file   TOURNAMENT.CPP
 *  @author Knut Felix J�rgensen
 *          Sebastian Opsahl
 *          Donia el Morabet
 *          Aron Julian Heimonen
 *          Andreas Birger Svendsen Watne
 *
 */




#include <iostream>                 //cout/cin
#include <string>                  //string
#include <vector>                  //vector
#include <fstream>                 //ifstream, ofstream
#include <list>                    //list
#include <cstdlib>
#include <algorithm>
#include <cmath>
#include <cctype>
#include "LesData2.h"

using namespace std;
const int sSeed = 0;                //Global int
                                    //enum
enum tournamentSeeding {automatic,manual};

class Tournament{
    private:
    public:
        string name;
        int tournamentSize;
        tournamentSeeding seed;
        Tournament(ifstream & inn, string nameNr);
        void writeToFile(ofstream & ut);
    Tournament() {name = ""; }
    void readData();
    void writeData();
};


class Team{
    private:

    public:
        string teamName;
        Team()  {teamName = ""; }
        vector <bool> wins;
        int seed;
        Team(ifstream & inn);
        void writeToFile(ofstream & ut);
        void readData();
        void writeData();
};
                                    //global vectors
vector <Tournament*> gTournament;
vector <Team*> sATeam;
vector <Team*> gTeam;
vector <Team*> sTeam;


void writeWinner(int first,int last,int round);
void sortToAcendingSeed();
void tournamentMenu();
void createTournament();
void currentTournament();
void registerWin();
void readFromFile();
void writeToFile();

int main(){
    readFromFile();

    int kommando;

    cout << "TOURNAMENT HOME PAGE\n\t";
    tournamentMenu();

    kommando = lesInt("\nCommands",0,3);

    while(kommando != 0){
        switch(kommando){
            case 1: createTournament();         break;
            case 2: currentTournament();        break;
            case 3: registerWin();              break;
            default: tournamentMenu();          break;
        }

        kommando = lesInt("\nCommands",0,3);
    }
    writeToFile();
}

void tournamentMenu(){
    cout << "Usable commands:\n"
         << "\t1 - Create tournament\n"
         << "\t2 - See tournament bracket\n"
         << "\t3 - Register wins\n"
         << "\t0 - Quit\n";
}

/**
 *  Reads data into tournament class
 *
 */

void Tournament::readData(){
    char seeding;                           //reads in tournament name
    cout << "Tournament name: "; getline(cin, name);

    do{                                     //reads inn tournament size
        cout << "Tournament size(4,8,16): "; cin >> tournamentSize;
    cin.ignore();
    }
    while(tournamentSize != 4 &&
           tournamentSize != 8 &&
           tournamentSize != 16);
    do {                                    //Asks if seeding is automatic or manual
        seeding = lesChar("Automatic/Manual seeding (A/M)");
    }

    while (seeding != 'A' && seeding != 'M');

    if (seeding == 'A') {seed = automatic;}

    else if(seeding == 'M') {seed = manual;}
}

/**
 *  Reads data into Team class
 *
 */

void Team::readData(){                      //reads a new team name
    cout << "Team name: ";  getline(cin, teamName);
    if (gTournament[gTournament.size()-1]->seed == manual) {

        cout << "Team seed: "; cin >> seed;     //reads the teams seed

        cin.ignore();
    }

    if (gTournament[gTournament.size()-1]->seed == automatic)
        seed = sSeed;
}

/**
 *  Writes out the tournament name
 *
 */
void Tournament::writeData(){
    cout << "Tournament name: " << name;
}

/**
 *  Writes out the Team names
 *
 */
void Team::writeData(){
    cout << "Team name: " << teamName;
}

/**
 *  Creates a new tournament
 *
 */
void createTournament(){
    char cValg;

    if (gTournament.size()==0) {
                            //Makes new tournament
        cout << "Create tournament\n";
        Tournament* newTournament = new Tournament();
        newTournament->readData();
        gTournament.push_back(newTournament);
                                //Creates the teams
        do {
            for(int i = 0; i < newTournament->tournamentSize; i++){
                Team* newTeam = new Team;
                newTeam->readData();
                gTeam.push_back(newTeam);
            }

            cout << "Are the teams created right (Y/N) ";
            cin >> cValg;
            cin.ignore();

            if (toupper(cValg) != 'Y') {
                for (int i = 0;i<gTeam.size();) {
                    gTeam.pop_back();
                    cout << gTeam[i]->teamName << " " << gTeam[i]->seed;
                }
            }
        }
        while (toupper(cValg) != 'Y');

        if (gTournament[0]->seed == manual) {
            sortToAcendingSeed();
        } else sTeam = gTeam;

        system("CLS");
        cout << '\n';
        tournamentMenu();

    } else cout << "Tournament already created ";
}

/**
 *  Prints the tournament brackets with standings based on Team data
 *
 */
void currentTournament(){
    system("CLS");                      //creating helping variables to keep
                                        //track of which round we are on
    int r2=2,r3=4,r4=8,r5=16,r6=32,rc2=0,rc3=1,rc4=3,rc5=7,rc6=15;
    cout << "Tournament bracket:\n\n";
    for (int i=0;i<gTeam.size();i++) {      //for loop through all the teams
                                            //gives error if no teams are created

                                            //Writes out all the teams
        cout << sTeam[i]->teamName << '\t' << '\n';
            if (!(i == gTeam.size()-1) ) {
        if (i==(rc5)) {cout << "\t\t\t\t";  //if round is 4 do four indents and
                                            //call on writeWinner function
        writeWinner(0,16,4);
        rc5+=r5;}
        if (i == rc4) {cout << "\t\t\t";    //if round is 3 do three indents and
                                            //call on writeWinner function
        writeWinner(i-3,i+4,3);
        rc4+=r4;}
        if (i == rc3) {cout << "\t\t";      //if round is 2 do two indents and
                                            //call on writeWinner function
        writeWinner(i-1,i+2,2);
        rc3+=r3;}
        if (i == rc2) {cout << '\t';        //if round is 1 do one indent and
                                            //call on writeWinner function
        writeWinner(i,i+1,1);
        rc2 +=r2;}
        }
        cout << '\n';
    }
    cout << '\n';
    tournamentMenu();
}

/**
 *  Registers wins on a chosen round
 *
 */
void registerWin(){                             //helping variables
    char cValg;
    int rounds = sqrt(gTeam.size()+1);
    int valg = 0;
    int rValg = 0;
    int aChosen = gTeam.size()+1;
    int aChosen2 = gTeam.size()+1;
    int buffer = 4;
    int i;
    bool alreadyRegistered;

    system("CLS");                              //clears screen
    gTournament[0]->writeData(); cout << '\n';


    rValg = lesInt("Choose round",1,rounds);

    for (i=0;i<gTeam.size();i++){
        if (gTeam[i]->wins.size() == rValg) {
                alreadyRegistered = true;
        } else if (alreadyRegistered != true) alreadyRegistered = false;
    }

    if (alreadyRegistered == false) {

        if (rValg == 1) {                           //if round is 1
            for(i = 0; i < gTeam.size(); i++){          //go from 0 through to last team
                cout << 1 << " "; sTeam[i]->writeData(); cout << '\n';
                cout << 2 << " "; sTeam[i+1]->writeData(); cout << '\n';
                valg = lesInt("Which team won?",1,2);
                if(valg == 1){                          //push back the corresponding winner
                                                        //and loser based on user choice
                    sTeam[i]->wins.push_back(true);
                    sTeam[i+1]->wins.push_back(false);
                }
                else if(valg == 2){
                    sTeam[i+1]->wins.push_back(true);
                    sTeam[i]->wins.push_back(false);
                }

                i++;
            }

            cout << '\n';

        }

        if (rValg == 2) {                               //if round is 2
            buffer = 4;                                 //buffer value for jumping
                                                        //to new match
            i = 0;

            do {                                            //run this until all teams
                                                            //have been checked
                                                            //checks is team has been
                                                            //registered correctly before
                for (i;i<buffer;i++) {
                    if (sTeam[i]->wins.size()==1 && sTeam[i]->wins[0]==true) {
                        if (i<aChosen) {aChosen = i; cout << 1;}
                        if (i>aChosen) {aChosen2 = i; cout << 2;}
                            cout << " ";  sTeam[i]->writeData(); cout << "\n";
                    }
                }

                buffer += 4;

                valg = lesInt("Which team won?",1,2);
                    if (valg == 1) {                        //push back the corresponding winner
                                                            //and loser based on user choice
                        sTeam[aChosen]->wins.push_back(true);
                        sTeam[aChosen2]->wins.push_back(false);

                    } else if (valg == 2) {
                        sTeam[aChosen2]->wins.push_back(true);
                        sTeam[aChosen]->wins.push_back(false);

                    }                                       //reset aChosen values

                        aChosen2 = gTeam.size()+1;
                        aChosen = gTeam.size()+1;
            } while (i<sTeam.size());
        }



        if (rValg == 3) {                               //if round is 2
            buffer = 8;                                 //buffer value for jumping
                                                        //to new match

            i = 0;

            do {                                            //run this until all teams
                                                            //have been checked
                                                            //check is if team has been
                                                            //registered correctly before
                for (i;i<buffer;i++) {
                    if (sTeam[i]->wins.size()==2 && sTeam[i]->wins[1]==true) {
                        if (i<aChosen) {aChosen = i; cout << 1;}
                        if (i>aChosen) {aChosen2 = i; cout << 2;}
                            cout << " ";  sTeam[i]->writeData(); cout << "\n";
                    }

                } buffer = 16;


                valg = lesInt("Which team won?",1,2);       //push back the corresponding winner
                                                            //and loser based on user choice
                if (valg == 1) {
                    sTeam[aChosen]->wins.push_back(true);
                    sTeam[aChosen2]->wins.push_back(false);

                } else if (valg == 2) {
                    sTeam[aChosen2]->wins.push_back(true);
                    sTeam[aChosen]->wins.push_back(false);

                }                                       //reset aChosen values

                    aChosen2 = gTeam.size()+1;
                    aChosen = gTeam.size()+1;
                } while (i<sTeam.size());
        }

        if (rValg == 4) {                               //if round is 4
            buffer = 16;                                //buffer value for determining
                                                        //scope of teams for one match

            i = 0;
            do {                                            //run this until all teams
                                                            //have been checked
                                                            //check is if team has been
                                                            //registered correctly before
                for (i;i<buffer;i++) {
                    if (sTeam[i]->wins.size()==3 && sTeam[i]->wins[2]==true) {
                        if (i<aChosen) {aChosen = i; cout << 1;}
                        if (i>aChosen) {aChosen2 = i; cout << 2;}
                            cout << " ";  sTeam[i]->writeData(); cout << "\n";

                    }
                }

                valg = lesInt("Which team won?",1,2);       //push back the corresponding winner
                                                            //and loser based on user choice
                    if (valg == 1) {
                        sTeam[aChosen]->wins.push_back(true);
                        sTeam[aChosen2]->wins.push_back(false);

                    } else if (valg == 2) {
                        sTeam[aChosen2]->wins.push_back(true);
                        sTeam[aChosen]->wins.push_back(false);

                    }
                                                           //reset aChosen values
                        aChosen2 = gTeam.size()+1;
                        aChosen = gTeam.size()+1;

                } while (i<sTeam.size());
        }

        cout << "Are the wins registered correctly? (Y/N) ";
        cin >> cValg;
        cin.ignore();

        if (toupper(cValg) != 'Y') {
            for (i=0;i<gTeam.size();i++){
                if (sTeam[i]->wins[rValg-1]==true || sTeam[i]->wins[rValg-1]==false) {
                    sTeam[i]->wins.pop_back();
                }
            }

        }
        system("CLS");
        tournamentMenu();
    } else {cout << "\nRound already registered ";

    system("CLS");
    tournamentMenu();}
    alreadyRegistered = false;

}


void readFromFile(){
    int buffer2=0;
    int buffer=0;
    string name;
    string winloss;
    ifstream innfil;
    string nameNr;
    vector <string> file_content;

    innfil.open("MAIN.DTA", ios::in);

    if(innfil.is_open()) {

        Tournament* newTournament;
        Team* newTeam;
        newTournament = new Tournament;
        innfil >> buffer2;

        if (buffer2 != 0) {
            innfil.ignore();
            getline(innfil,newTournament->name);
            innfil >> newTournament->tournamentSize;
            innfil >> buffer2;

            if (buffer2 == 0) {
                    newTournament->seed = automatic;
            } else {newTournament->seed = manual;}

            if (gTournament.size()==0 && (newTournament->seed == 0 || newTournament->seed == 1)) {
                gTournament.push_back(newTournament);
            }

            for (int i=0;i<newTournament->tournamentSize;i++) {
                newTeam = new Team;

                innfil >> newTeam->teamName;
                //    cout << "\nTeam: " << newTeam->teamName;
                innfil.ignore();
                innfil >> newTeam->seed;

                //    cout << "\nSeed " << newTeam->seed;
                innfil.ignore();
                getline(innfil,winloss);
                //    cout << "\nWinloss size" << winloss.size();
                for (int t=0; t < winloss.size();t++) {
                    if (winloss[t] == '1' || winloss[t] == '0') {
                        if (winloss[t] == '1') {newTeam->wins.push_back(1);}
                        if (winloss[t] == '0') {newTeam->wins.push_back(0);}
                    }

                }
                for (int t=0;t<newTeam->wins.size();t++){

                }

                gTeam.push_back(newTeam);

            }
            // cout << "\nGteam size " <<gTeam.size() << "\n";
            // for (int t=0;t<gTeam.size();t++) {
            //        cout << "\nTeam: " <<gTeam[t]->teamName;
            //        cout << "\nSeed:" << gTeam[t]->seed;
            //        cout << "\nWinloss:" << gTeam[t]->wins[0];
            //    }
        }

        if (gTournament.size()!=0) sortToAcendingSeed();

        cout << "\n";
        innfil.close();


    } else {
        cout << "\nCant find the file\n";
    }
}


void writeToFile(){
    ofstream utfil("MAIN.DTA");
    utfil << gTournament.size() << "\n";

    for(int i = 0; i < gTournament.size(); i++){
        gTournament[i]->writeToFile(utfil);
        utfil << "\n";
    }

    utfil.close();
}

Tournament::Tournament(ifstream & inn, string nameNr){
int enumen;
    name = nameNr;
    inn >> tournamentSize;
    inn >> enumen;

    if(enumen == 0){
        seed = automatic;
    } else{
        seed = manual;
    }

    for(int i = 0; i < tournamentSize; i++){
        cin.ignore();
        gTeam.push_back(new Team(inn));
    }
}


void Tournament::writeToFile(ofstream & ut){
    ut << name; ut << "\n";
    ut << tournamentSize;  ut << "\n";

    int sUt = 0;

    if (gTournament[0] -> seed == 0) {
        sUt = 0;
    }
    else {
        sUt = 1;
    }

    ut << sUt;

    for(int i = 0; i < tournamentSize; i++){
        ut << "\n";
        gTeam[i]->writeToFile(ut);
    }
}

Team::Team(ifstream & inn){
    int import;
    inn >> teamName;
    inn >> seed;

    for(int i = 0; i < 4; i++){
        cout << " ";
        inn >> import;

        if (import == 1) wins[i] = true;
        else wins[i] = false;
    }
}

void Team::writeToFile(ofstream & ut){
    ut << teamName << '\n';
    ut << seed << '\n';

    for(int i = 0; i < wins.size(); i++){
            ut << wins[i];
    }
}

/**
 *  Creates copies data from gTeam to sATeam sorts it by seeding size
 *  and pushes back the correct bracket form to sTeam
 *
 */


void sortToAcendingSeed(){
    int tournamentSize;                         //Help variables
    int seedCheck;
    int counter = 0;
    vector <int> ao;

    tournamentSize = gTeam.size();
                                                //goes through all teams
                                                //checks if seeding is automatic
    for (int i=0;i<gTeam.size()-1;i++){
        counter += gTeam[i]->seed;
    }

    if (counter>0) {



                                                //pushes back seeding values
                                                //into help vector
        for (int i=0;i<gTeam.size();i++) {
            ao.push_back(gTeam[i]->seed);
        }

        sort(ao.begin(),ao.end());                  //sorts data low to high

        for (int i=0;i<gTeam.size();i++) {          //goes through all the teams
                                                    //and pushes back the teams in
                                                    //acending order in sATeam
            for (int t=0;t<gTeam.size();t++){
                if (ao[i] == gTeam[t]->seed) {
                    sATeam.push_back(gTeam[t]);
                }

            }

        }
                                                    //pushes back tournament based
                                                    //on best matchup seeding practices
                                                    //for teams of 4 or 8 or 16
        if (tournamentSize == 4){
            sTeam.push_back(sATeam[0]);
            sTeam.push_back(sATeam[3]);
            sTeam.push_back(sATeam[1]);
            sTeam.push_back(sATeam[2]);
        }

        if (tournamentSize == 8){
            sTeam.push_back(sATeam[0]);
            sTeam.push_back(sATeam[7]);
            sTeam.push_back(sATeam[3]);
            sTeam.push_back(sATeam[4]);
            sTeam.push_back(sATeam[1]);
            sTeam.push_back(sATeam[6]);
            sTeam.push_back(sATeam[2]);
            sTeam.push_back(sATeam[5]);
        }

        if (tournamentSize == 16){
            sTeam.push_back(sATeam[0]);
            sTeam.push_back(sATeam[15]);
            sTeam.push_back(sATeam[7]);
            sTeam.push_back(sATeam[8]);
            sTeam.push_back(sATeam[3]);
            sTeam.push_back(sATeam[12]);
            sTeam.push_back(sATeam[4]);
            sTeam.push_back(sATeam[11]);
            sTeam.push_back(sATeam[1]);
            sTeam.push_back(sATeam[14]);
            sTeam.push_back(sATeam[6]);
            sTeam.push_back(sATeam[9]);
            sTeam.push_back(sATeam[2]);
            sTeam.push_back(sATeam[13]);
            sTeam.push_back(sATeam[5]);
            sTeam.push_back(sATeam[10]);

        }

    } else {

    sTeam = gTeam;

    }

}

/**
 *  Writes out the winner based on the scope sent in to the function
 *
 * @param first - start array place to look for teams
 * @param last - last array place to look for teams
 * @param round - The array place of the wins vector to check
 */


void writeWinner(int first,int last,int round){
    int i = first;
    int t = last;
    int check = 0;
    int buffer = 0;
                                            //checks if round is 1 and that winners
                                            //are registered
                                            //writes out the name of the team found
    if (round == 1 && !(sTeam[first]->wins.size()==0)) {
        if (sTeam[first]->wins[0]==true) cout << sTeam[first]->teamName;
        else if (sTeam[first]->wins[0]==false) cout << sTeam[last]->teamName;
    } else if (sTeam[first]->wins.size()==0 && round < 2) cout << "TBD ";

                                            //checks if round is 2 and that winners
    int getWinnerInt(int round);            //are registered on a team that is within the scope
    if (round == 2 && round!= 3 && round != 4 && (sTeam[first]->wins.size()>=2 || sTeam[first + 1]->wins.size()>=2 )){
        for (i;i<t+1;i++) {
                                            //reads the wins vector and stores away wins
                                            //for each team
                                            //if a team has won all its matches for the round
                                            //writes its name

            for (int q=0;q<round;q++){
                if (sTeam[i]->wins[q]==true)
                    check++;
                if (check == round) cout << sTeam[i]->teamName;
                buffer = i;
            }
        check = 0;
        }                                   //if check failes write TBD
    } else if (sTeam[buffer]->wins.size()<2 && (round < 3 || round > 2) && round != 1 && round !=3 && round !=4)
        cout << "TBD ";

                                            //checks if round is 3 and that winners
                                            //are registered on a team that is within the scope
    if (round == 3 && round !=2 && round != 4 &&
        (sTeam[first]->wins.size()>=3 || sTeam[first+1]->wins.size()>=3 ||
        sTeam[first+2]->wins.size()>=3 || sTeam[first+3]->wins.size()>=3)){
        for (i;i<t+1;i++) {                 //reads the wins vector and stores away wins
                                            //for each team
                                            //if a team has won all its matches for the round
                                            //writes its name

            for (int q=0;q<round;q++){
                if (sTeam[i]->wins[q]==true)
                    check++;
                if (check == round) cout << sTeam[i]->teamName;
                buffer = i;
            }
        check = 0;
        }                                   //if check failes write TBD
    } else if (sTeam[buffer]->wins.size()<3 && (round < 4 || round > 3) && round != 1 && round != 2 && round != 4) {
        cout << "TBD ";
    }

                                            //checks if round is 4 and that winners
                                            //are registered on a team that is within the scope
    if (round == 4 && round != 3 && round != 2 && round != 1 &&
        (sTeam[first]->wins.size()>=4 || sTeam[first+1]->wins.size()>=4 ||
        sTeam[first+2]->wins.size()>=4 || sTeam[first+4]->wins.size()>=4 ||
        sTeam[first+4]->wins.size()>=4 || sTeam[first+5]->wins.size()>=4 ||
        sTeam[first+6]->wins.size()>=4 || sTeam[first+7]->wins.size()>=4)){

        for (i;i<t;i++) {                   //reads the wins vector and stores away wins
                                            //for each team
                                            //if a team has won all its matches for the round
                                            //writes its name
                if (sTeam[i]->wins[3]==true) cout << sTeam[i]->teamName;
        }
                                            //if check failes write TBD
    } else if (sTeam[buffer]->wins.size()<4 && (round < 5 || round > 4)
                            && round != 1 && round != 2 && round != 3) {
        cout << "TBD ";
    }

}
