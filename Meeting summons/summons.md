## Meeting Notices

Hello, Everyone

I am sending you this meeting notice for the meeting to be held at: Friday 25.02.2022, 11:15 - 13:30 in A159
If you are unable to attend the meeting at the designated time please give send me a message before the meeting.

Agenda:
Work on Iteration 1, Project task, vision template 


---

Hello, Everyone

I am sending you this meeting notice for the meeting to be held at: Monday 28.02.2022, 15:00 - 17:30 to be held digitaly
If you are unable to attend the meeting at the designated time please give send me a message before the meeting.

Agenda:
Work meeting to go to work on the delivery of the iteration

---

Hello, Everyone

I am sending you this meeting notice for the meeting to be held at: Monday 28.02.2022, 17:30 - 18:00 to be held digitaly
If you are unable to attend the meeting at the designated time please give send me a message before the meeting.

Agenda:
Initial costumer meeting introductions and explanation. 

---

Hello, Everyone

I am sending you this meeting notice for the meeting to be held at: thursday 03.03.2022, 15:00 - 18:30 to be held digitaly
If you are unable to attend the meeting at the designated time please give send me a message before the meeting.

Agenda:
Meeting to sum up the work done on the first iteration

---

Hello, Everyone

I am sending you this meeting notice for the meeting to be held at: Friday 07.03.2022, 14:00 - 14:30 to be held digitaly
If you are unable to attend the meeting at the designated time please give send me a message before the meeting.

Agenda:
Have a short meeting about the current wireframe and user tests

---

Hello, Everyone

I am sending you this meeting notice for the meeting to be held at: Friday 09.03.2022, 18:15 - 18:45 to be held physically K112
If you are unable to attend the meeting at the designated time please give send me a message before the meeting.

 
Agenda:
Have a short meeting about the current wireframe and user tests

---

Hello, Everyone

I am sending you this meeting notice for the meeting to be held at: Friday 07.03.2022, 15:30 - 16:15 to be held at A161
If you are unable to attend the meeting at the designated time please give send me a message before the meeting.

Agenda:
Preform a user test on our costumer

---

Hello, Everyone

I am sending you this meeting notice for the meeting to be held at: wednesday 18.03.2022, 14:15 - 16:00 digitaly
If you are unable to attend the meeting at the designated time please give send me a message before the meeting.

Agenda:
Weekly TA meeting to go through the motions and bring up any concerns we have toward the second iteration

---

Hello, Everyone

I am sending you this meeting notice for the meeting to be held at: thursday 17.03.2022, 14:15 - 16:00 digitaly
If you are unable to attend the meeting at the designated time please give send me a message before the meeting.

Agenda:
In this meeting we are to go through and document the requirements and limitations set for the production of the MVP.

---

Hello, Everyone

I am sending you this meeting notice for the meeting to be held at: Friday 18.03.2022, 14:15 - 16:00 digitaly
If you are unable to attend the meeting at the designated time please give send me a message before the meeting.

Agenda:
Start the work towards deliverables of second iteration

---

Hello, Everyone

I am sending you this meeting notice for the meeting to be held at: monday 21.03.2022, 14:00 - 17:00 in Ametyst: A159
If you are unable to attend the meeting at the designated time please give send me a message before the meeting.

Agenda:
The meeting will be a work meeting to work together towards the production of our MVP. The work wil be based on the tasks given in the last meeting and specifics about the tasks can be found in microsoft planner.

---

Hello, Everyone

I am sending you this meeting notice for the meeting to be held at: Wednesday 23.03.2022, 08:00 - 11:30 in Ametyst: A159
If you are unable to attend the meeting at the designated time please give send me a message before the meeting.

Agenda:
Continuation of work started on monday with our MVP. 

---

Weekly TA meeting - week 12

Hello,

Time for our weekly TA meeting: Friday 25.03.2022. 

//Donia

---

Weekly TA meeting - week 13

Hi all,

Calling in to our weekly TA meeting. We will discuss the process until the last and final submittance of the project, that is only a few weeks away now. 

Notice that the meeting will be held both physically and through teams, for the ones that does not have the opportunity to attend in person. 

See you at room A270, or online on teams on thursday 29.03.2022!

//Donia

---

Weekly TA meeting week 14

Hi, all!

Calling us in to our weekly TA meeting. Reflections and question about the last iteration and presentations will be this weeks topic. 

Like last time, it will be held both physically at room A266 and on teams, thursday 07.04.2022. 

See you on thursday!

Br,
Donia

---

2.client meeting

All,

Sending you this meeting notice for the 2.client meeting to be held at: Friday 08.04.2022, 14:50 - 15:10 in A232.

The agenda for this meeting will be:

MVP.
User test of the MVP
Sequence diagram
Improved wireframe based on the user test conducted. 
First version of Wiki
Time log
Gannt chart

See you all at the meeting.

Br,
Donia

---

Weekly TA meeting - week 16

Hi everyone!

Hope you all had a wonderful easter holiday and are back more energized. 

Its time for our weekly TA meeting. AS usual, if you have any questions or concerns, now will be the time to bring this up. 

This meeting will be held on teams only, as i have not booked a room this time. 

Br,
Donia

---


Hello, Everyone

I am sending you this meeting notice for the meeting to be held at: Tuesday 26.04.2022, 12:30 - 14:00 to be held digitaly

If you are unable to attend the meeting at the designated time please give send me a message before the meeting.

Agenda: Work on iteration 3.

---


Hello, team.

Now that we are only a few days away from the deadline on Friday, april 29th, 15:00 pm, I am inviting us all for a 4 hour work meeting. Let's focus on the remaining tasks we have left and help each other out if we have questions and concerns.

See you all on teams tomorrow.

Br,
Donia


---
