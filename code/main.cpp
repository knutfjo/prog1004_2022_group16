#include <iostream>                 //cout/cin
#include <string>                  //string
#include <vector>                  //vector
#include <fstream>                 //ifstream, ofstream
#include <list>                    //list
#include <cstdlib>
#include "LesData2.h"

using namespace std;

const int sSeed = 300;

enum tournamentSeeding {automatic,manual};

class Tournament{
    private:
        string name;
    public:
        int tournamentSize;
        tournamentSeeding seed;
        Tournament(ifstream & inn, string nameNr);
        void writeToFile(ofstream & ut);
        
    Tournament() {name = ""; }

    void readData();
    void writeData();
};

class Team{
    private:
    public:
        string teamName;
        Team()  {teamName = ""; }
        vector <bool> wins;
        int seed;
        Team(ifstream & inn);
        void writeToFile(ofstream & ut);

void readData();
void writeData();
};

vector <Tournament*> gTournament;
vector <Team*> gTeam;

int rounds = 0;

void tournamentMenu();
void createTournament();
void currentTournament();
void registerWin();
void readFromFile();
void writeToFile();

int main(){
    readFromFile();
    int kommando;
    cout << "TOURNAMENT HOME PAGE\n\t";
    tournamentMenu();
    kommando = lesInt("\nCommands",0,3);
    while(kommando != 0){
        switch(kommando){
            case 1: createTournament();         break;
            case 2: currentTournament();        break;
            case 3: registerWin();              break;
            default: tournamentMenu();          break;
        }
        kommando = lesInt("\nCommands",0,3);
    }

    writeToFile();

}

void tournamentMenu(){
    cout << "Usable commands:\n"
         << "\t1 - Create tournament\n"
         << "\t2 - See tournament bracket\n"
         << "\t3 - Register wins\n"
         << "\t0 - Quit\n";
}

void Tournament::readData(){
    char seeding;
    cout << "Tournament name: "; getline(cin, name);
    do{
    cout << "Tournament size(4,8,16): "; cin >> tournamentSize;
    cin.ignore();
    }while(tournamentSize != 4 &&
           tournamentSize != 8 &&
           tournamentSize != 16);
    do {
    seeding = lesChar("Automatic/Manual seeding (A/M)");
    } while (seeding != 'A' && seeding != 'M');
    if (seeding=='A') seed = automatic;
    
    else seed = manual;
}

void Team::readData(){
    cout << "Team name: ";  getline(cin, teamName);

    if (gTournament[gTournament.size()-1]->seed == 1) {
    cout << "Team seed: "; cin >> seed;
    cin.ignore();}

    if (gTournament[gTournament.size()-1]->seed == 0)
        seed = sSeed;
}

void Tournament::writeData(){
    cout << "Tournament name: " << name;
}

void Team::writeData(){
    cout << "Team name: " << teamName;
}

void createTournament(){
                            //Makes new tournament
    cout << "Create tournament\n";
    Tournament* newTournament = new Tournament();
    newTournament->readData();
    gTournament.push_back(newTournament);
                            //Creates the teams
    for(int i = 0; i < newTournament->tournamentSize; i++){
        Team* newTeam = new Team;
        newTeam->readData();
        gTeam.push_back(newTeam);
    }

    system("CLS");
    cout << '\n';
    tournamentMenu();
}

void currentTournament(){
    system("CLS");
    int r2=2,r3=4,r4=8,r5=16,r6=32,rc2=0,rc3=1,rc4=3,rc5=7,rc6=15;
    string tbd = "TBD";
    cout << "Tournament bracket:\n\n";

    for (int i=0;i<gTeam.size();i++) {
        cout << gTeam[i]->teamName << '\t' << '\n';
            if (!(i == gTeam.size()-1) ) {
            if (i==(rc5)) {cout << "\t\t\t\t" << tbd;
            rc5+=r5;}
            if (i == rc4) {cout << "\t\t\t" << tbd;
            rc4+=r4;}
            if (i == rc3) {cout << "\t\t" << tbd;
            rc3+=r3;}
            if (i == rc2) {cout << '\t' << tbd;
            rc2 +=r2;}
            }

    cout << '\n';
    }

    cout << '\n';
    tournamentMenu();

}

void registerWin(){
    int rounds = 0;
    int valg = 0;

    system("CLS");
    gTournament[0]->writeData(); cout << '\n';

    for(int i = 0; i < gTeam.size(); i++){
        cout << 1 << " "; gTeam[i]->writeData(); cout << '\n';
        cout << 2 << " "; gTeam[i+1]->writeData(); cout << '\n';
        valg = lesInt("Which team won?",1,2);
        if(valg == 1){
            gTeam[i]->wins.push_back(true);
            gTeam[i+1]->wins.push_back(false);
        }
        else if(valg == 2){
            gTeam[i+1]->wins.push_back(true);
            gTeam[i]->wins.push_back(false);
        }
        i++;
    }

    cout << "Results from round " << rounds+1 << '\n';

    for(int i = 0; i < gTeam.size(); i++){
        gTeam[i]->writeData(); cout << " - ";
            if(gTeam[i]->wins[rounds] == true) cout << "WIN\n";
            else cout << "LOST\n";
        }

        cout << '\n';
        tournamentMenu();

}

void readFromFile(){
    ifstream innfil("MAIN.DTA");

    int amountTournament = 0;

    if(innfil){
        innfil >> amountTournament;
   
        for(int i = 0; i < amountTournament; i++){
            string nameNr;
            getline(innfil,nameNr);
            gTournament.push_back(new Tournament(innfil,nameNr));
            cin.ignore();
        }

        innfil.close();
    } 
    
    else{
        cout << "\nFinner ikke filen\n";
    }
}

void writeToFile(){
    ofstream utfil("MAIN.DTA");

    utfil << gTournament.size() << "\n";

    for(int i = 0; i < gTournament.size(); i++){
        gTournament[i]->writeToFile(utfil);
        utfil << "\n";
    }

    utfil.close();
}

Tournament::Tournament(ifstream & inn, string nameNr){
    name = nameNr; 

    inn >> tournamentSize;

    for(int i = 0; i < tournamentSize; i++){
        gTeam.push_back(new Team(inn));
    }
}

void Tournament::writeToFile(ofstream & ut){
    ut << name; //ut << "\n";
    ut << tournamentSize;

    for(int i = 0; i < tournamentSize; i++){
        ut << "\n";
        gTeam[i]->writeToFile(ut);
    }
}

Team::Team(ifstream & inn){
    int import;
    inn >> teamName;
    inn >> seed;

    for(int i = 0; i < 4; i++){
        cout << " ";
        inn >> import; 
        if (import == 1) wins[i] = true;
        else wins[i] = false;
    }
}

void Team::writeToFile(ofstream & ut){
    ut << teamName << ' ';
    ut << seed;

    for(int i = 0; i < wins.size(); i++){
        ut << ' ' << wins[i];
    }
    /*
    for(int i = 0; i < teamMembers.size(); i++){
        ut << teamMembers[i];
    }
    */
}